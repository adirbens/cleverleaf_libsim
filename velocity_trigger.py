from visit import *
import os
from time import sleep
from constants import *



FILE_NAME = "velocity_trigger"
X_COORD = 5.5



def TriggerOccure(minimum_x_coord, obj):
    if obj != None and obj['max_coord'][0] > minimum_x_coord and GetEngineList():
        return True
    return False


def HandleTrigger(time, minimum_x_coord, obj, cycle):

    with open(ALL_PATHS_PREFIX + OUT_PATH_SUFFIX, "a") as trigger_file:
        s = "# VELOCITY TRIGGER OCCURED! #\n"
        s += f"velocity minimum x coordinate value: {minimum_x_coord}\n"
        s += f"cycle: {cycle}, time: {time}, velocity maximum value: {obj['max']}, at x cordinate: {obj['max_coord'][0]}\n\n"
        trigger_file.write(s)

    if os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX):
        os.remove(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX)

    SendSimulationCommand(host_name, simfile, "png", PNG_BUTTON + FILE_NAME)

    while not os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX) and GetEngineList():
        continue



AddPlot("Pseudocolor", "velocity_magnitude")
DrawPlots()
SetActivePlots(0)


if os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX):
    os.remove(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX)

SendSimulationCommand(host_name, simfile, "update", UPDATE_BUTTON)

while not os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX) and GetEngineList():
    continue

if GetEngineList():
    Query("Time")
    t = GetQueryOutputValue()
    Query("Max")
    maxObj = GetQueryOutputObject()
    Query("Cycle")
    c = GetQueryOutputValue()

    if TriggerOccure(X_COORD, maxObj):
        HandleTrigger(t, X_COORD, maxObj, c)

DeleteAllPlots()


with open(ALL_PATHS_PREFIX + FILE_NAME, "w"):
    pass
