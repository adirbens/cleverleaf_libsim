ALL_PATHS_PREFIX = "/home/adirbens/"

OUT_PATH_SUFFIX = "projects/CleverLeaf_ref/trigger_out_file.txt"
SIM_PATH_SUFFIX = ".visit/simulations/"
TRIGGERS_PATH_SUFFIX = "projects/CleverLeaf_ref/trigger_scripts/"

TRIGGER_CHECK_PATH_SUFFIX = "trigger_check_file"
PNG_PATH_SUFFIX = "trigger_png_file"
UPDATE_PATH_SUFFIX = "trigger_update_file"

RUN_BUTTON = "clicked();run;QPushButton;Simulations;NONE"
PNG_BUTTON = "clicked();png;QPushButton;Simulations;"
UPDATE_BUTTON = "clicked();update;QPushButton;Simulations;NONE"

SLEEP_TIME = 0.1