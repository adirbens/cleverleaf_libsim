from visit import *
from socket import gethostname
import os
from pathlib import Path
from time import sleep
from constants import *



if os.path.exists(ALL_PATHS_PREFIX + OUT_PATH_SUFFIX):
    os.remove(ALL_PATHS_PREFIX + OUT_PATH_SUFFIX)



triggers = [file_name[:file_name.index('.')] for file_name in os.listdir(ALL_PATHS_PREFIX + TRIGGERS_PATH_SUFFIX)]

simfile_path = ALL_PATHS_PREFIX + SIM_PATH_SUFFIX
paths = sorted(Path(simfile_path).iterdir(), key=os.path.getmtime)
simfile = str(paths[-1])
host_name = gethostname()

LaunchNowin()
OpenDatabase(simfile)


while GetEngineList():

    while not os.path.exists(ALL_PATHS_PREFIX + TRIGGER_CHECK_PATH_SUFFIX) and GetEngineList():
        sleep(SLEEP_TIME)

    
    for trigger in triggers:

        if os.path.exists(ALL_PATHS_PREFIX + trigger):
            os.remove(ALL_PATHS_PREFIX + trigger)

        Source(ALL_PATHS_PREFIX + TRIGGERS_PATH_SUFFIX + trigger + ".py")
        
        while not os.path.exists(ALL_PATHS_PREFIX + trigger) and GetEngineList():
            sleep(SLEEP_TIME)


    if os.path.exists(ALL_PATHS_PREFIX + TRIGGER_CHECK_PATH_SUFFIX):
        os.remove(ALL_PATHS_PREFIX + TRIGGER_CHECK_PATH_SUFFIX)

    SendSimulationCommand(host_name, simfile, "run", RUN_BUTTON)


# ClearAllWindows()
# CloseComputeEngine(host_name, simfile)
# CloseDatabase(simfile)
Close()
