#include "libsimDataWriter.h"


static int run_mode = SIM_STOPPED;
static int GetRunMode() { return run_mode; }
static void SetRunMode(int new_run_mode) { run_mode = new_run_mode; }

static LibsimDataStructures* libDataStructures;

static LibsimMesh libmesh;
static LibsimCoords coords;
static vector<LibsimVar> vars;
static vector<LibsimExpr> exprs;

void LibsimDataWriter::LibsimInitialize(const char* name, const char* description, int rank, int world_size, LibsimDataStructures* dataStructures, int dim){

    libDataStructures = dataStructures;

    char *env = NULL;
    VisItSetupEnvironment();

    VisItSetBroadcastIntFunction2(visit_broadcast_int_callback, nullptr);
    VisItSetBroadcastStringFunction2(visit_broadcast_string_callback, nullptr);

    VisItSetParallel(world_size > 1);
    VisItSetParallelRank(rank);

    if (rank == 0)
        env = VisItGetEnvironment();

    VisItSetupEnvironment2(env);
    if (env != NULL)
        free(env);

    if (rank == 0)
    {
        VisItInitializeSocketAndDumpSimFile(
            name,
            description,
            "/path/to/where/sim/was/started",
            NULL, NULL, NULL);
    }

    RegisterLibsimData(dim);
}

void LibsimDataWriter::RegisterLibsimData(int dim){

    libmesh = {         "AMR_mesh",
                        dim,
                        VISIT_MESHTYPE_AMR};

    coords = {          "vertexcoords",
                        dim};

    exprs.push_back({   "garbage_expr",
                        "density - velocity_magnitude",
                        VISIT_VARTYPE_SCALAR});

    vars.push_back({    "pressure",
                        1,
                        VISIT_VARTYPE_SCALAR,
                        VISIT_VARCENTERING_ZONE});
    vars.push_back({    "density",
                        1,
                        VISIT_VARTYPE_SCALAR,
                        VISIT_VARCENTERING_ZONE});
    vars.push_back({    "viscosity",
                        1,
                        VISIT_VARTYPE_SCALAR,
                        VISIT_VARCENTERING_ZONE});
    vars.push_back({    "velocity",
                        dim,
                        VISIT_VARTYPE_VECTOR,
                        VISIT_VARCENTERING_NODE});
}

void LibsimDataWriter::LibsimHandleVisitState(int rank)
{
    SetRunMode(SIM_STOPPED);
    int visit_state;
    ofstream MyFile(TRIGGER_CHECK_PATH);
    MyFile.close();

    while (GetRunMode() == SIM_STOPPED)
    {
        if (rank == 0)
            visit_state = VisItDetectInput(1, fileno(stdin));
        MPI_Bcast(&visit_state, 1, MPI_INT, 0, MPI_COMM_WORLD);

        switch (visit_state)
        {
            case TIMEOUT:
                break;
            case CONNECTION:
                LibsimHandleConnection(rank);
                break;
            case ENGINE:
                LibsimHandleEngine(rank);
                break;
            case CONSOLE:
                LibsimHandleInput(rank);
                break;
            default:
                break;
        }
    }
}

void LibsimDataWriter::LibsimHandleConnection(int rank){
    if (VisItAttemptToCompleteConnection() == VISIT_OKAY)
    {
        if (rank == 0)
            fprintf(stderr, "VisIt connected\n");

        VisItSetCommandCallback(ControlCommandCallback, libDataStructures);
        VisItSetSlaveProcessCallback2(SlaveProcessCallback, libDataStructures);

        VisItSetGetMetaData(SimGetMetaData, libDataStructures);
        VisItSetGetMesh(SimGetMesh, libDataStructures);
        VisItSetGetVariable(SimGetVariable, libDataStructures);
        VisItSetGetDomainNesting(SimGetDomainNesting, libDataStructures);

        VisItSetGetDomainList(SimGetDomainList, libDataStructures);
    }
    else
    {
        char *err = VisItGetLastError();
        fprintf(stderr, "VisIt did not connect: %s\n", err);
        free(err);
    }
}
void LibsimDataWriter::LibsimHandleEngine(int rank){
    if (!ProcessVisItCommand(rank))
    {
        VisItDisconnect();
        SetRunMode(SIM_RUNNING);
    }
}
void LibsimDataWriter::LibsimHandleInput(int rank){
    /* No reason to support console input for the simulation. */
}
        
void LibsimDataWriter::ControlCommandCallback(const char *cmd, const char *args, void *cbdata)
{
    LibsimDataStructures* libsim_data_structures = (LibsimDataStructures*)cbdata;

    if (strcmp(cmd, "run") == 0)
        SetRunMode(SIM_RUNNING);
    else if (strcmp(cmd, "update") == 0){
        VisItTimeStepChanged();
        VisItUpdatePlots();

        ofstream MyFile(TRIGGER_UPDATE_PATH);
        MyFile.close();
    }
    else if (strcmp(cmd, "png") == 0){  // ASSUME THAT PLOTS ARE UPDATED
        char* name = GetPngName(args);
        char filename[100];
        sprintf(filename, "cleverleaf_%s_%06d.png", name, libsim_data_structures->lagrangian_eulerian_integrator->getIntegratorStep());
        VisItSaveWindow(filename, 1000, 1000, VISIT_IMAGEFORMAT_PNG);

        ofstream MyFile(TRIGGER_PNG_PATH);
        MyFile.close();
    }
    else
        fprintf(stderr, "cmd=%s, args=%s\n", cmd, args);
}

visit_handle LibsimDataWriter::SimGetMetaData(void *cbdata)
{
    visit_handle md = VISIT_INVALID_HANDLE;
    LibsimDataStructures* libsim_data_structures = (LibsimDataStructures*)cbdata;
    shared_ptr<hier::PatchHierarchy> patch_hierarchy = libsim_data_structures->patch_hierarchy;
    shared_ptr<LagrangianEulerianIntegrator> lagrangian_eulerian_integrator = libsim_data_structures->lagrangian_eulerian_integrator;

    if (VisIt_SimulationMetaData_alloc(&md) == VISIT_OKAY)
    {
        visit_handle mmd = VISIT_INVALID_HANDLE;

        VisIt_SimulationMetaData_setMode(md, (GetRunMode() == SIM_STOPPED) ? VISIT_SIMMODE_STOPPED : VISIT_SIMMODE_RUNNING);
        VisIt_SimulationMetaData_setCycleTime(md, lagrangian_eulerian_integrator->getIntegratorStep(), lagrangian_eulerian_integrator->getIntegratorTime());

        if (VisIt_MeshMetaData_alloc(&mmd) == VISIT_OKAY)
        {
            VisIt_MeshMetaData_setName(mmd, libmesh.name);
            VisIt_MeshMetaData_setMeshType(mmd, libmesh.type);
            VisIt_MeshMetaData_setTopologicalDimension(mmd, libmesh.dim);
            VisIt_MeshMetaData_setSpatialDimension(mmd, libmesh.dim);

            int ndoms = 0, curr_doms;
            int nlevels = patch_hierarchy->getNumberOfLevels();
            for (int i = 0; i < nlevels; i++)
            {
                curr_doms = patch_hierarchy->getPatchLevel(i)->getNumberOfPatches();
                for (int j = 0; j < curr_doms; j++)
                {
                    char tmpName[100];
                    sprintf(tmpName, "level%d,patch%d", i, j);
                    VisIt_MeshMetaData_addDomainName(mmd, tmpName);
                    VisIt_MeshMetaData_addGroupId(mmd, i);
                }
                ndoms += curr_doms;
            }

            VisIt_MeshMetaData_setNumDomains(mmd, ndoms);
            VisIt_MeshMetaData_setDomainTitle(mmd, "patches");
            VisIt_MeshMetaData_setDomainPieceName(mmd, "patch");

            VisIt_MeshMetaData_setNumGroups(mmd, nlevels);
            VisIt_MeshMetaData_setGroupTitle(mmd, "levels");
            VisIt_MeshMetaData_setGroupPieceName(mmd, "level");

            VisIt_SimulationMetaData_addMesh(md, mmd);
        }

        for (LibsimVar var : vars)
        {
            visit_handle vmd = VISIT_INVALID_HANDLE;
            if (VisIt_VariableMetaData_alloc(&vmd) == VISIT_OKAY)
            {
                VisIt_VariableMetaData_setName(vmd, var.name);
                VisIt_VariableMetaData_setMeshName(vmd, libmesh.name);
                VisIt_VariableMetaData_setType(vmd, var.type);
                VisIt_VariableMetaData_setCentering(vmd, var.centering);
                VisIt_SimulationMetaData_addVariable(md, vmd);
            }
        }
        
        for (LibsimExpr expr : exprs)
        {
            visit_handle emd = VISIT_INVALID_HANDLE;
            if(VisIt_ExpressionMetaData_alloc(&emd) == VISIT_OKAY)
            {
                VisIt_ExpressionMetaData_setName(emd, expr.name);
                VisIt_ExpressionMetaData_setDefinition(emd, expr.definition);
                VisIt_ExpressionMetaData_setType(emd, expr.type);
                VisIt_SimulationMetaData_addExpression(md, emd);
            }
        }
        
        const char *cmd_names[] = {"run", "update", "png"};
        for (size_t i = 0; i < sizeof(cmd_names) / sizeof(const char *); i++)
        {
            visit_handle cmd = VISIT_INVALID_HANDLE;
            if (VisIt_CommandMetaData_alloc(&cmd) == VISIT_OKAY)
            {
                VisIt_CommandMetaData_setName(cmd, cmd_names[i]);
                VisIt_SimulationMetaData_addGenericCommand(md, cmd);
            }
        }
    }
    return md;
}

visit_handle LibsimDataWriter::SimGetMesh(int domain, const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;
    LibsimDataStructures* libsim_data_structures = (LibsimDataStructures*)cbdata;
    SAMRAI::hier::Patch* patch = DomainToPatch(domain, libsim_data_structures->patch_hierarchy.get());

    if (strcmp(name, libmesh.name) == 0 && patch != NULL)
    {
        if (VisIt_CurvilinearMesh_alloc(&h) != VISIT_ERROR)
        {
            string name_coords = coords.name;
            shared_ptr<clever::pdat::CleverNodeData<double>> cellcoords(
                SHARED_PTR_CAST(clever::pdat::CleverNodeData<double>,
                                patch->getPatchData(patch->getPatchDescriptor()->
                                    mapNameToIndex(name_coords + "##CURRENT"))));

            int xStart, xEnd, xGhostWidth, xVarNum, xRealVarNum;
            int yStart, yEnd, yGhostWidth, yVarNum, yRealVarNum;
            int zStart = 0, zEnd = 0, zGhostWidth = 0, zVarNum = 1, zRealVarNum = 1;

            hier::Box ghostBox = cellcoords->getGhostBox();
            hier::IntVector ghostWidth = cellcoords->getGhostCellWidth();

            xStart = ghostBox.lower(0);
            xEnd = ghostBox.upper(0) + NODE_CENTERED_ADDITION;
            xGhostWidth = ghostWidth[0];

            yStart = ghostBox.lower(1);
            yEnd = ghostBox.upper(1) + NODE_CENTERED_ADDITION;
            yGhostWidth = ghostWidth[1];

            if (coords.dim == 3)
            {
                zStart = ghostBox.lower(2);
                zEnd = ghostBox.upper(2) + NODE_CENTERED_ADDITION;
                zGhostWidth = ghostWidth[2];
            }

            xVarNum = xEnd - xStart;
            yVarNum = yEnd - yStart;
            if (coords.dim == 3)
                zVarNum = zEnd - zStart;

            xRealVarNum = xVarNum - 2 * xGhostWidth;
            yRealVarNum = yVarNum - 2 * yGhostWidth;
            if (coords.dim == 3)
                zRealVarNum = zVarNum - 2 * zGhostWidth;

            visit_handle xc, yc, zc;
            VisIt_VariableData_alloc(&xc);
            VisIt_VariableData_alloc(&yc);
            VisIt_VariableData_alloc(&zc);
            if (xc != VISIT_INVALID_HANDLE && yc != VISIT_INVALID_HANDLE && zc != VISIT_INVALID_HANDLE)
            {
                int max[3], min[3], dim[3];
                dim[0] = xVarNum;
                dim[1] = yVarNum;
                dim[2] = 0;
                min[0] = cellcoords->getGhostCellWidth()[0];
                min[1] = cellcoords->getGhostCellWidth()[1];
                min[2] = 0;
                max[0] = xVarNum - 1 - xGhostWidth;
                max[1] = yVarNum - 1 - yGhostWidth;
                max[2] = 0;

                // VisIt_VariableData_setDataD(xc, VISIT_OWNER_SIM, 1, xVarNum * yVarNum * zVarNum, cellcoords->getPointer(0));
                // VisIt_VariableData_setDataD(yc, VISIT_OWNER_SIM, 1, xVarNum * yVarNum * zVarNum, cellcoords->getPointer(1));
                double* xData = getDataNoGhosts(cellcoords->getPointer(0), yRealVarNum, yGhostWidth, xRealVarNum, xGhostWidth, zRealVarNum, zGhostWidth);
                double* yData = getDataNoGhosts(cellcoords->getPointer(1), yRealVarNum, yGhostWidth, xRealVarNum, xGhostWidth, zRealVarNum, zGhostWidth);

                dim[0] = xRealVarNum;
                dim[1] = yRealVarNum;

                VisIt_VariableData_setDataD(xc, VISIT_OWNER_VISIT, 1, yRealVarNum * xRealVarNum * zRealVarNum, xData);
                VisIt_VariableData_setDataD(yc, VISIT_OWNER_VISIT, 1, yRealVarNum * xRealVarNum * zRealVarNum, yData);
                //////////////////

                if (coords.dim == 2)
                    VisIt_CurvilinearMesh_setCoordsXY(h, dim, xc, yc); 
                else if (coords.dim == 3)
                {
                    dim[2] = zVarNum;
                    min[2] = cellcoords->getGhostCellWidth()[2];
                    max[2] = zVarNum - 1 - zGhostWidth;

                    // VisIt_VariableData_setDataD(zc, VISIT_OWNER_SIM, 1, xVarNum * yVarNum * zVarNum, cellcoords->getPointer(2));
                    double* zData = getDataNoGhosts(cellcoords->getPointer(2), yRealVarNum, yGhostWidth, xRealVarNum, xGhostWidth, zRealVarNum, zGhostWidth);

                    dim[2] = zRealVarNum;

                    VisIt_VariableData_setDataD(zc, VISIT_OWNER_VISIT, 1, yRealVarNum * xRealVarNum * zRealVarNum, zData);
                    ///////////////////

                    VisIt_CurvilinearMesh_setCoordsXYZ(h, dim, xc, yc, zc);
                }

                // VisIt_CurvilinearMesh_setRealIndices(h, min, max);
            }
        }
    }
    return h;
}
visit_handle LibsimDataWriter::SimGetVariable(int domain, const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;
    LibsimDataStructures* libsim_data_structures = (LibsimDataStructures*)cbdata;
    SAMRAI::hier::Patch* patch = DomainToPatch(domain, libsim_data_structures->patch_hierarchy.get());
    shared_ptr<hier::PatchDescriptor> patch_descriptor = patch->getPatchDescriptor();

    if (patch != NULL){

        for (LibsimVar var : vars)
        {
            if (strcmp(name, var.name) == 0)
            {
                string name = var.name;
                int xStart, xEnd, xGhostWidth, xVarNum, xRealVarNum;
                int yStart, yEnd, yGhostWidth, yVarNum, yRealVarNum;
                int zStart = 0, zEnd = 0, zGhostWidth = 0, zVarNum = 1, zRealVarNum = 1;

                if (var.centering == VISIT_VARCENTERING_ZONE)
                {
                    shared_ptr<clever::pdat::CleverCellData<double>> data(
                        SHARED_PTR_CAST(clever::pdat::CleverCellData<double>,
                                        patch->getPatchData(patch_descriptor->mapNameToIndex(name + "##CURRENT"))));

                    hier::Box ghostBox = data->getGhostBox();
                    hier::IntVector ghostWidth = data->getGhostCellWidth();

                    xStart = ghostBox.lower(0);
                    xEnd = ghostBox.upper(0) + CELL_CENTERED_ADDITION;
                    xGhostWidth = ghostWidth[0];

                    yStart = ghostBox.lower(1);
                    yEnd = ghostBox.upper(1) + CELL_CENTERED_ADDITION;
                    yGhostWidth = ghostWidth[1];

                    if (coords.dim == 3)
                    {
                        zStart = ghostBox.lower(2);
                        zEnd = ghostBox.upper(2) + NODE_CENTERED_ADDITION;
                        zGhostWidth = ghostWidth[2];
                    }

                    xVarNum = xEnd - xStart;
                    yVarNum = yEnd - yStart;
                    if (coords.dim == 3)
                        zVarNum = zEnd - zStart;

                    xRealVarNum = xVarNum - 2 * xGhostWidth;
                    yRealVarNum = yVarNum - 2 * yGhostWidth;
                    if (coords.dim == 3)
                        zRealVarNum = zVarNum - 2 * zGhostWidth;
            
                    VisIt_VariableData_alloc(&h);
                    // VisIt_VariableData_setDataD(h, VISIT_OWNER_SIM, 1, xVarNum * yVarNum * zVarNum, data->getPointer());
                    double* dataNoGhosts = getDataNoGhosts(data->getPointer(), yRealVarNum, yGhostWidth, xRealVarNum, xGhostWidth, zRealVarNum, zGhostWidth);
                    VisIt_VariableData_setDataD(h, VISIT_OWNER_VISIT, 1, yRealVarNum * xRealVarNum * zRealVarNum, dataNoGhosts);
                    //////////////////
                }
                else if (var.centering == VISIT_VARCENTERING_NODE)
                {
                    shared_ptr<clever::pdat::CleverNodeData<double>> data(
                        SHARED_PTR_CAST(clever::pdat::CleverNodeData<double>,
                                        patch->getPatchData(patch_descriptor->mapNameToIndex(name + "##CURRENT"))));

                    hier::Box ghostBox = data->getGhostBox();
                    hier::IntVector ghostWidth = data->getGhostCellWidth();

                    xStart = ghostBox.lower(0);
                    xEnd = ghostBox.upper(0) + NODE_CENTERED_ADDITION;
                    xGhostWidth = ghostWidth[0];

                    yStart = ghostBox.lower(1);
                    yEnd = ghostBox.upper(1) + NODE_CENTERED_ADDITION;
                    yGhostWidth = ghostWidth[1];

                    if (coords.dim == 3)
                    {
                        zStart = ghostBox.lower(2);
                        zEnd = ghostBox.upper(2) + NODE_CENTERED_ADDITION;
                        zGhostWidth = ghostWidth[2];
                    }

                    xVarNum = xEnd - xStart;
                    yVarNum = yEnd - yStart;
                    if (coords.dim == 3)
                        zVarNum = zEnd - zStart;

                    xRealVarNum = xVarNum - 2 * xGhostWidth;
                    yRealVarNum = yVarNum - 2 * yGhostWidth;
                    if (coords.dim == 3)
                        zRealVarNum = zVarNum - 2 * zGhostWidth;

                    // double* xData = data->getPointer(0);
                    // double* yData = data->getPointer(1);
                    double* xDataNoGhosts = getDataNoGhosts(data->getPointer(0), yRealVarNum, yGhostWidth, xRealVarNum, xGhostWidth, zRealVarNum, zGhostWidth);
                    double* yDataNoGhosts = getDataNoGhosts(data->getPointer(1), yRealVarNum, yGhostWidth, xRealVarNum, xGhostWidth, zRealVarNum, zGhostWidth); 
                    double* zDataNoGhosts = nullptr;

                    if (coords.dim == 3)
                        zDataNoGhosts = getDataNoGhosts(data->getPointer(2), yRealVarNum, yGhostWidth, xRealVarNum, xGhostWidth, zRealVarNum, zGhostWidth);
                        // zData = data->getPointer(2);
                    
                    // double* dataMix = mergeArrays(xVarNum * yVarNum * zVarNum, var.dim, xData, yData, zData);
                    double* dataMixNoGhosts = mergeArrays(xRealVarNum * yRealVarNum * zRealVarNum, var.dim, xDataNoGhosts, yDataNoGhosts, zDataNoGhosts);

                    VisIt_VariableData_alloc(&h);
                    // VisIt_VariableData_setDataD(h, VISIT_OWNER_VISIT, var.dim, xVarNum * yVarNum * zVarNum, dataMix);
                    VisIt_VariableData_setDataD(h, VISIT_OWNER_VISIT, var.dim, xRealVarNum * yRealVarNum * zRealVarNum, dataMixNoGhosts);
                }
                break;
            }
        }
    }
    return h;
}

visit_handle LibsimDataWriter::SimGetDomainNesting(const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;
    LibsimDataStructures* libsim_data_structures = (LibsimDataStructures*)cbdata;
    hier::PatchHierarchy* patch_hierarchy = libsim_data_structures->patch_hierarchy.get();

    if (VisIt_DomainNesting_alloc(&h) != VISIT_ERROR)
    {
        int ratios[3];
        int npatches = 0, domain = 0;
        int logicalExtents[6];
        int nlevels = patch_hierarchy->getNumberOfLevels();
        for (int i = 0; i < nlevels; i++)
            npatches += patch_hierarchy->getPatchLevel(i)->getNumberOfPatches();

        VisIt_DomainNesting_set_dimensions(h, npatches, nlevels, 2);
        
        for (int level = 0; level < nlevels; ++level)
        {
            hier::PatchLevel* patchLevel = patch_hierarchy->getPatchLevel(level).get();
            if (level == 0)
                ratios[0] = ratios[1] = ratios[2] = 1;
            else
            {
                hier::IntVector ratio = patchLevel->getRatioToCoarserLevel();
                ratios[0] = ratio[0];
                ratios[1] = ratio[1];
                ratios[2] = 1;
            }
            VisIt_DomainNesting_set_levelRefinement(h, level, ratios);
            
            npatches = patchLevel->getNumberOfPatches();
            for (int i = 0; i < npatches; i++)
            {
                hier::Box curr_box = DomainToBox(domain, patch_hierarchy);
                logicalExtents[0] = curr_box.lower(0);
                logicalExtents[1] = curr_box.lower(1);
                logicalExtents[2] = 0;
                logicalExtents[3] = curr_box.upper(0);
                logicalExtents[4] = curr_box.upper(1);
                logicalExtents[5] = 0;

                int n_child;
                int *child;
                child = DomainToChild(domain, level, &n_child, patch_hierarchy);
                VisIt_DomainNesting_set_nestingForPatch(h, domain++, level, child, n_child, logicalExtents);
            }
        }  
    }
    return h;
}

visit_handle LibsimDataWriter::SimGetDomainList(const char *name, void *cbdata)
{
    visit_handle h = VISIT_INVALID_HANDLE;
    LibsimDataStructures* libsim_data_structures = (LibsimDataStructures*)cbdata;
    hier::PatchHierarchy* patch_hierarchy = libsim_data_structures->patch_hierarchy.get();

    if (VisIt_DomainList_alloc(&h) != VISIT_ERROR)
    {
        visit_handle hdl = VISIT_INVALID_HANDLE;
        int npatches_global = 0;
        for (int i = 0; i < patch_hierarchy->getNumberOfLevels(); i++)
            npatches_global += patch_hierarchy->getPatchLevel(i)->getNumberOfPatches();

        int local_pnum = LocalPatchesNum(patch_hierarchy);
        if (local_pnum > 0)
        {
            int* local_plist = LocalPatchesList(local_pnum, patch_hierarchy);
            VisIt_VariableData_alloc(&hdl);
            VisIt_VariableData_setDataI(hdl, VISIT_OWNER_VISIT, 1, local_pnum, local_plist);
        }

        VisIt_DomainList_setDomains(h, npatches_global, hdl);
    }
    return h;
}

/* Process commands from viewer on all processors. */
int LibsimDataWriter::ProcessVisItCommand(int rank)
{
    int command = 0;
    if (rank == 0)
    {
        int success = VisItProcessEngineCommand();

        if (success == VISIT_OKAY)
        {
            command = VISIT_COMMAND_SUCCESS;
            BroadcastSlaveCommand(&command, nullptr);
            return 1;
        }
        else
        {
            command = VISIT_COMMAND_FAILURE;
            BroadcastSlaveCommand(&command, nullptr);
            return 0;
        }
    }
    else
    {
        /* Note: only through the SlaveProcessCallback callback
        * above can the rank 0 process send a VISIT_COMMAND_PROCESS
        * instruction to the non-rank 0 processes. */
        while (1)
        {
            BroadcastSlaveCommand(&command, nullptr);
            switch (command)
            {
                case VISIT_COMMAND_PROCESS:
                    VisItProcessEngineCommand();
                    break;
                case VISIT_COMMAND_SUCCESS:
                    return 1;
                case VISIT_COMMAND_FAILURE:
                    return 0;
            }
        }
    }
}

hier::Patch* LibsimDataWriter::DomainToPatch(int domain, hier::PatchHierarchy *patch_hierarchy)
{
    int patch_level = 0;
    while (patch_level < patch_hierarchy->getNumberOfLevels())
    {
        shared_ptr<hier::PatchLevel> patchLevel = patch_hierarchy->getPatchLevel(patch_level);
        if (patchLevel->getNumberOfPatches() > domain)
        {
            for (int i = 0; i < patchLevel->getLocalNumberOfPatches(); i++)
                if (patchLevel->getPatch(i)->getLocalId() == domain)
                    return patchLevel->getPatch(i).get();
            return nullptr;
        }
        else
        {
            domain -= patchLevel->getNumberOfPatches();
            patch_level++;
        }
    }
    return nullptr;
}

hier::Box LibsimDataWriter::DomainToBox(int domain, hier::PatchHierarchy* patch_hierarchy)
{
    int patch_level = 0;
    while (patch_level < patch_hierarchy->getNumberOfLevels())
    {
        shared_ptr<hier::PatchLevel> patchLevel = patch_hierarchy->getPatchLevel(patch_level);
        if (patchLevel->getNumberOfPatches() > domain)
        {
            hier::BoxContainer container = patchLevel->getBoxes();
            for (hier::BoxContainer::iterator ip(container.begin()); ip != container.end(); ++ip)
            {
                const hier::Box box = *ip;
                if (box.getLocalId() == domain)
                    return box;
            }
        }
        else
        {
            domain -= patchLevel->getNumberOfPatches();
            patch_level++;
        }
    }
    return DomainToBox(domain, patch_hierarchy); // TO FIX, SHOULD NEVER REACH HERE
}

int LibsimDataWriter::isBoxInside(hier::Box small_box, hier::Box big_box, hier::IntVector ratio)
{
    big_box.refine(ratio);
    if (big_box.intersects(small_box))
        return 1;
    return 0;
}

int LibsimDataWriter::BoxToDomain(hier::Box box, hier::PatchHierarchy* patch_hierarchy, int level)
{
    int domain = 0;
    int patch_level = 0;

    while (patch_level < level)
        domain += patch_hierarchy->getPatchLevel(patch_level++)->getNumberOfPatches();

    domain += box.getLocalId().getValue();
    return domain;
}

int* LibsimDataWriter::DomainToChild(int domain, int level, int* childSize, hier::PatchHierarchy* patch_hierarchy)
{
    *childSize = 0;
    int *child;

    if (level == patch_hierarchy->getMaxNumberOfLevels() - 1)
    {
        child = (int *)malloc(sizeof(int));
        child[0] = -1;
        return child;
    }

    shared_ptr<hier::PatchLevel> patchLevel = patch_hierarchy->getPatchLevel(level + 1);
    hier::BoxContainer container = patchLevel->getBoxes();
    hier::Box domainBox = DomainToBox(domain, patch_hierarchy);
    hier::IntVector ratio = patchLevel->getRatioToCoarserLevel();
    for (hier::BoxContainer::iterator ip(container.begin()); ip != container.end(); ++ip)
    {
        hier::Box box = *ip;
        if (isBoxInside(box, domainBox, ratio))
            (*childSize)++;
    }

    if ((*childSize) == 0)
    {
        child = (int *)malloc(sizeof(int));
        child[0] = -1;
        return child;
    }

    child = (int *)malloc(sizeof(int) * (*childSize));
    int index = 0;
    container = patchLevel->getBoxes();
    for (hier::BoxContainer::iterator ip(container.begin()); ip != container.end(); ++ip)
    {
        hier::Box box = *ip;
        if (isBoxInside(box, domainBox, ratio))
            child[index++] = BoxToDomain(box, patch_hierarchy, level + 1);
    }
    return child;
}

int LibsimDataWriter::LocalPatchesNum(hier::PatchHierarchy* patch_hierarchy)
{
    int npatches = 0;
    for (int i = 0; i < patch_hierarchy->getNumberOfLevels(); i++)
        npatches += patch_hierarchy->getPatchLevel(i)->getLocalNumberOfPatches();
    return npatches;
}

int* LibsimDataWriter::LocalPatchesList(int size, hier::PatchHierarchy* patch_hierarchy)
{
    int* p_list = (int *)malloc(size * sizeof(int));
    int domain_offset = 0;
    int index = 0;
    for (int i = 0; i < patch_hierarchy->getNumberOfLevels(); i++)
    {
        shared_ptr<hier::PatchLevel> patchLevel = patch_hierarchy->getPatchLevel(i);
        for (int j = 0; j < patchLevel->getLocalNumberOfPatches(); j++)
            p_list[index++] = domain_offset + patchLevel->getPatch(j)->getLocalId().getValue();
        domain_offset += patchLevel->getNumberOfPatches();
    }
    return p_list;
}

double* LibsimDataWriter::getDataNoGhosts(double* data, int rows, int y_ghost_width, int cols, int x_ghost_width, int height, int z_ghost_width)
{
    double* dataNoGhosts = (double *)malloc(sizeof(double) * rows * cols * height);
    for (int k = 0; k < height; k++)
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                dataNoGhosts[k * cols * rows + i * cols + j] = data[(k + z_ghost_width) * (cols + 2 * x_ghost_width) * (rows + 2 * y_ghost_width) + (i + y_ghost_width) * (cols + 2 * x_ghost_width) + (j + x_ghost_width)];
    return dataNoGhosts;
}

double* LibsimDataWriter::mergeArrays(int size, int dim, double* x, double* y, double* z)
{
    double* mergedArray = (double*)malloc(sizeof(double) * size * dim);
    int index = 0;
    for (int i = 0; i < size; i++)
    {
        mergedArray[index++] = x[i];
        mergedArray[index++] = y[i];
        if (z != nullptr)
        {
            mergedArray[index++] = z[i];
        } 
    }
    return mergedArray;
}

char* LibsimDataWriter::GetPngName(const char * command)
{
    string cmd = command;
    int pos = cmd.find_last_of(';');
    char* name = (char*)malloc(strlen(command) - pos);
    strcpy(name, &command[pos + 1]);
    return name;
}

int* LibsimDataWriter::tagGhosts(int rows, int cols, int ghostY, int ghostX)
{
    int* ghostTags = (int *)malloc(sizeof(int) * rows * cols);
    for (int y = 0; y < rows; y++)
    {
        for (int x = 0; x < cols; x++)
        {
            if (y >= ghostY && y <= rows - 1 - ghostY && x >= ghostX && x <= cols - 1 - ghostX)
            {
                ghostTags[y * cols + x] = VISIT_GHOSTCELL_REAL;
            }
            else
            {
                ghostTags[y * cols + x] = VISIT_GHOSTCELL_REAL;
            }
        }
    }
    return ghostTags;
}