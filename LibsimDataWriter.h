#include "hydro/LagrangianEulerianIntegrator.h"
#include "SAMRAI/hier/PatchHierarchy.h"
#include "SAMRAI/appu/VisItDataWriter.h"
#include "hydro/Cleverleaf.h"

#include <vector>

#include <VisItControlInterface_V2.h>
#include <VisItDataInterface_V2.h>

using namespace std;


#define SIM_STOPPED 0
#define SIM_RUNNING 1

#define VISIT_COMMAND_PROCESS 0
#define VISIT_COMMAND_SUCCESS 1
#define VISIT_COMMAND_FAILURE 2

#define TRIGGER_CHECK_PATH "/home/adirbens/trigger_check_file"
#define TRIGGER_UPDATE_PATH "/home/adirbens/trigger_update_file"
#define TRIGGER_PNG_PATH "/home/adirbens/trigger_png_file"

#define TIMEOUT 0
#define CONNECTION 1
#define ENGINE 2
#define CONSOLE 3

#define CELL_CENTERED_ADDITION 1
#define NODE_CENTERED_ADDITION 2



struct LibsimDataStructures {
    std::shared_ptr<hier::PatchHierarchy> patch_hierarchy;
    std::shared_ptr<LagrangianEulerianIntegrator> lagrangian_eulerian_integrator;
};

struct LibsimMesh{
    const char* name;
    int dim;
    VisIt_MeshType type;
};
struct LibsimCoords{
    const char* name;
    int dim;
};
struct LibsimVar{
    const char* name;
    int dim;
    VisIt_VarType type;
    VisIt_VarCentering centering;
};
struct LibsimExpr{
    const char* name;
    const char* definition;
    VisIt_VarType type;
};


class LibsimDataWriter
{
    public:
        
        static void LibsimInitialize(const char* name, const char* description, int rank, int world_size, LibsimDataStructures* dataStructures, int dim);
        static void LibsimHandleVisitState(int rank);
        

    private:

        static int visit_broadcast_int_callback(int *value, int sender,void *cbdata)
        {
            return MPI_Bcast(value, 1, MPI_INT, sender, MPI_COMM_WORLD);
        }
        static int visit_broadcast_string_callback(char *str, int len, int sender, void *cbdata)
        {
            return MPI_Bcast(str, len, MPI_CHAR, sender, MPI_COMM_WORLD);
        }
        static void BroadcastSlaveCommand(int *command, void *cbdata)
        {
            MPI_Bcast(command, 1, MPI_INT, 0, MPI_COMM_WORLD);
        }
        static void SlaveProcessCallback(void *cbdata)
        {
            int command = VISIT_COMMAND_PROCESS;
            BroadcastSlaveCommand(&command, cbdata);
        }

        static void RegisterLibsimData(int dim);

        static void LibsimHandleConnection(int rank);
        static void LibsimHandleEngine(int rank);
        static void LibsimHandleInput(int rank);

        static int ProcessVisItCommand(int rank);
        static void ControlCommandCallback(const char *cmd, const char *args, void *cbdata);

        static visit_handle SimGetMetaData(void *cbdata);
        static visit_handle SimGetMesh(int domain, const char *name, void *cbdata);
        static visit_handle SimGetVariable(int domain, const char *name, void *cbdata);
        static visit_handle SimGetDomainNesting(const char *name, void *cbdata);
        static visit_handle SimGetDomainList(const char *name, void *cbdata);

        static hier::Patch* DomainToPatch(int domain, hier::PatchHierarchy* patch_hierarchy);
        static hier::Box DomainToBox(int domain, hier::PatchHierarchy* patch_hierarchy);
        static int isBoxInside(hier::Box small_box, hier::Box big_box, hier::IntVector ratio);
        static int BoxToDomain(hier::Box box, hier::PatchHierarchy* patch_hierarchy, int level);
        static int* DomainToChild(int domain, int level, int* childSize, hier::PatchHierarchy* patch_hierarchy);
        static int LocalPatchesNum(hier::PatchHierarchy* patch_hierarchy);
        static int* LocalPatchesList(int size, hier::PatchHierarchy* patch_hierarchy);
        static double* getDataNoGhosts(double *data, int rows, int y_ghost_width, int cols, int x_ghost_width, int height=1, int z_ghost_width=0);
        static double* mergeArrays(int size, int dim, double* x, double* y, double* z = nullptr);
        static char* GetPngName(const char * command);
        static int* tagGhosts(int rows, int cols, int ghostY, int ghostX);
};
