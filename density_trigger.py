from visit import *
import os
from time import sleep
from constants import *



FILE_NAME = "density_trigger"
DENSITY_VAL = 50000


def TriggerOccure(minimum_density_val, density):
    if density >= minimum_density_val:
        return True
    return False


def HandleTrigger(time, minimum_density_val, density, cycle):

    with open(ALL_PATHS_PREFIX + OUT_PATH_SUFFIX, "a") as trigger_file:
        s = "# DENSITY TRIGGER OCCURED! #\n"
        s += f"total density passed minimum value: {minimum_density_val}\n"
        s += f"cycle: {cycle}, time: {time}, total density value: {density}\n\n"
        trigger_file.write(s)

    if os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX):
        os.remove(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX)

    SendSimulationCommand(host_name, simfile, "png", PNG_BUTTON + FILE_NAME)

    while not os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX) and GetEngineList():
        sleep(SLEEP_TIME)



AddPlot("Pseudocolor", "density")
DrawPlots()
SetActivePlots(0)


if os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX):
    os.remove(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX)

SendSimulationCommand(host_name, simfile, "update", UPDATE_BUTTON)

while not os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX) and GetEngineList():
    sleep(SLEEP_TIME)

if GetEngineList():
    Query("Time")
    t = GetQueryOutputValue()
    Query("Variable Sum")
    sum = GetQueryOutputValue()
    Query("Cycle")
    c = GetQueryOutputValue()

    if TriggerOccure(DENSITY_VAL, sum):
        HandleTrigger(t, DENSITY_VAL, sum, c)

DeleteAllPlots()


with open(ALL_PATHS_PREFIX + FILE_NAME, "w"):
    pass
