from visit import *
import os
from time import sleep
from constants import *



FILE_NAME = "pressure_trigger"
PRESSURE_VAL = 450000


def TriggerOccure(minimum_pressure_val, pressure):
    if pressure >= minimum_pressure_val:
        return True
    return False


def HandleTrigger(time, minimum_pressure_val, pressure, cycle):

    with open(ALL_PATHS_PREFIX + OUT_PATH_SUFFIX, "a") as trigger_file:
        s = "# PRESSURE TRIGGER OCCURED! #\n"
        s += f"total pressure passed minimum value: {minimum_pressure_val}\n"
        s += f"cycle: {cycle}, time: {time}, total pressure value: {pressure}\n\n"
        trigger_file.write(s)

    if os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX):
        os.remove(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX)

    SendSimulationCommand(host_name, simfile, "png", PNG_BUTTON + FILE_NAME)

    while not os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX) and GetEngineList():
        sleep(SLEEP_TIME)



AddPlot("Pseudocolor", "pressure")
DrawPlots()
SetActivePlots(0)


if os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX):
    os.remove(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX)

SendSimulationCommand(host_name, simfile, "update", UPDATE_BUTTON)

while not os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX) and GetEngineList():
    sleep(SLEEP_TIME)

if GetEngineList():
    Query("Time")
    t = GetQueryOutputValue()
    Query("Variable Sum")
    sum = GetQueryOutputValue()
    Query("Cycle")
    c = GetQueryOutputValue()

    if TriggerOccure(PRESSURE_VAL, sum):
        HandleTrigger(t, PRESSURE_VAL, sum, c)

DeleteAllPlots()


with open(ALL_PATHS_PREFIX + FILE_NAME, "w"):
    pass
