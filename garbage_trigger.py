from visit import *
import os
from time import sleep
from constants import *


FILE_NAME = "garbage_trigger"
GARBAGE_MIN_VAL = 20000
GARBAGE_MAX_VAL = 30000


def TriggerOccure(minimum_garbage_val, maximux_garbage_val, garbage):
    if maximux_garbage_val >= garbage >= minimum_garbage_val:
        return True
    return False


def HandleTrigger(time, minimum_garbage_val, maximux_garbage_val, garbage, cycle):

    with open(ALL_PATHS_PREFIX + OUT_PATH_SUFFIX, "a") as trigger_file:
        s = "# GARBAGE TRIGGER OCCURED! #\n"
        s += f"garbage value between minimum value: {minimum_garbage_val} and maximum value: {maximux_garbage_val}\n"
        s += f"cycle: {cycle}, time: {time}, total garbage value: {garbage}\n\n"
        trigger_file.write(s)

    if os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX):
        os.remove(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX)

    SendSimulationCommand(host_name, simfile, "png", PNG_BUTTON + FILE_NAME)

    while not os.path.exists(ALL_PATHS_PREFIX + PNG_PATH_SUFFIX) and GetEngineList():
        sleep(SLEEP_TIME)



AddPlot("Pseudocolor", "garbage_expr")
DrawPlots()
SetActivePlots(0)


if os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX):
    os.remove(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX)

SendSimulationCommand(host_name, simfile, "update", UPDATE_BUTTON)

while not os.path.exists(ALL_PATHS_PREFIX + UPDATE_PATH_SUFFIX) and GetEngineList():
    sleep(SLEEP_TIME)

if GetEngineList():
    Query("Time")
    t = GetQueryOutputValue()
    Query("Variable Sum")
    sum = GetQueryOutputValue()
    Query("Cycle")
    c = GetQueryOutputValue()

    if TriggerOccure(GARBAGE_MIN_VAL, GARBAGE_MAX_VAL, sum):
        HandleTrigger(t, GARBAGE_MIN_VAL, GARBAGE_MAX_VAL, sum, c)

DeleteAllPlots()


with open(ALL_PATHS_PREFIX + FILE_NAME, "w"):
    pass
